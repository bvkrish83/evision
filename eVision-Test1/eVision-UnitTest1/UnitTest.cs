﻿using eVision_Test1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace eVision_UnitTest1
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            int accountId = 1234;

            IAccountService accountService = AccountService.GetAccountService();
            AccountInfo accountInfo = new AccountInfo(accountId, accountService);

            var expected = accountService.GetAccountAmount(accountId);
            
            accountInfo.RefreshAmount();
            var actual = accountInfo.Amount;

            Assert.IsTrue(expected == actual, "RefreshAmount result is not as expected.");
        }
    }
}
