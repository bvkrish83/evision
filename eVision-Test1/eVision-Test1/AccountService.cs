﻿using System.Diagnostics;

namespace eVision_Test1
{
    // Dummy implementation of IAccountService for testing purposes.
    public class AccountService : IAccountService
    {
        // Private instance of AccountService object.
        private static IAccountService _accountService = null;

        // Make this class as a Singleton object.
        private AccountService()
        {
        }

        // Public method to get hold of IAccountService singleton instance.
        public static IAccountService GetAccountService()
        {
            // Lazily initialize _acccountService singleton.
            if (_accountService == null)
            {
                _accountService = new AccountService();
            }
            return _accountService;
        }

        // Assumptions (for simplicity):
        // 1. Multiple calls to GetAccountAmount would return same value.
        public double GetAccountAmount(int accountId)
        {
            // Generate and return a value based on accountId.
            return accountId << accountId;
        }
    }

    public interface IAccountService
    {
        double GetAccountAmount(int accountId);
    }

    public class AccountInfo
    {
        private readonly int _accountId;
        private readonly IAccountService _accountService;

        public AccountInfo(int accountId, IAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }

        public double Amount { get; private set; }

        public void RefreshAmount()
        {
            Amount = _accountService.GetAccountAmount(_accountId);
        }
    }
}
