﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using eVision_Test2;

namespace eVision_UnitTest2
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            int accountId = 1234;

            IAccountService accountService = AccountService.GetAccountService();
            AccountInfo accountInfo = new AccountInfo(accountId, accountService);

            var expected = accountService.GetAccountAmount(accountId).Result;

            accountInfo.RefreshAmount().Wait();
            var actual = accountInfo.Amount;

            Assert.IsTrue(expected == actual, "RefreshAmount result is not as expected.");
        }
    }
}
