﻿using System.Threading;
using System.Threading.Tasks;

namespace eVision_Test2
{
    // Dummy implementation of IAccountService for testing purposes.
    public class AccountService : IAccountService
    {
        // Private instance of AccountService object.
        private static IAccountService _accountService = null;

        // Make this class as a Singleton object.
        private AccountService()
        {
        }

        // Public method to get hold of IAccountService singleton instance.
        public static IAccountService GetAccountService()
        {
            // Lazily initialize _acccountService singleton.
            if (_accountService == null)
            {
                _accountService = new AccountService();
            }
            return _accountService;
        }

        // Assumptions (for simplicity):
        // 1. Multiple calls to GetAccountAmount would return same value.
        public async Task<double> GetAccountAmount(int accountId)
        {
            // introduce a delay to make compiler happy. :)
            await Task.Delay(10);
            
            // Generate and return a value based on accountId.
            return accountId << accountId;
        }
    }

    public interface IAccountService
    {
        Task<double> GetAccountAmount(int accountId);
    }

    public class AccountInfo
    {
        private readonly int _accountId;
        private readonly IAccountService _accountService;

        public AccountInfo(int accountId, IAccountService accountService)
        {
            _accountId = accountId;
            _accountService = accountService;
        }

        public double Amount { get; private set; }

        // Int32 flag to make GetAccountAmount method re-entrant
        // 0 - No concurrent instances executing
        // 1 - Some concurrent instances executing
        private int inFunction = 0;

        // Boolean flag to identify whether an active instance
        // of RefreshAmount currently executing or not.
        private bool isCurrentlyRefreshing = false;

        // Re-entrant version of RefreshAmount method.
        // inFunction == 0 - executes and refreshes Amount corresponding to account.
        // inFunction == 1 - No-op, as the other running instances would update Amount.
        public async Task RefreshAmount()
        {
            if (Interlocked.CompareExchange(ref inFunction, 1, 0) == 0)
            {
                // No other instances of RefreshAmount is currently executing
                // Go ahead and fetch the refreshed amount.
                try
                {
                    isCurrentlyRefreshing = true;
                    Amount = await _accountService.GetAccountAmount(_accountId);
                }
                finally
                {
                    inFunction = 0;
                    isCurrentlyRefreshing = false;
                }
            }
            else
            {
                // Previous instances of RefreshAmount currently executing.
                // So, wait until previous refresh calls complete and then return.
                while (isCurrentlyRefreshing) ;
            }
        }
    }
}
